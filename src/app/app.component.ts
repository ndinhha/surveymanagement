import { Component, ViewEncapsulation } from '@angular/core';
// import { MainComponent } from './containers';

@Component({
  selector: 'app',
  styleUrls: [
    './app.component.scss'
  ],
  encapsulation: ViewEncapsulation.None,
  template: `
    <div class="app">
      <router-outlet></router-outlet>
    </div>
  `
})
export class AppComponent {}
