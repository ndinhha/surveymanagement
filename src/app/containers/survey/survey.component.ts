import { Component, ViewEncapsulation } from '@angular/core';
import { AppBar } from '../../ui';
import { ToolList, Section } from '../../models';
import { EventService } from '../../services';
import $ from 'jquery';

declare var mathEditor: any;
declare var com: any;

@Component({
  selector: 'survey-container',
  styleUrls: ['./survey.scss'],
  templateUrl: './survey.html',
  encapsulation: ViewEncapsulation.None,
})
export class SurveyComponent {
  isShowLibrary = false;
  sections: Array<Section> = [];
  currentActiveTab = 0;

  mathEditor;

  constructor(
    private eventService: EventService
  ){
    this.eventService.subscribe(this.onToolSelected.bind(this));

    setTimeout(() => {
      this.mathEditor = com.wiris.jsEditor.JsEditor.newInstance({'language': 'en'});
      this.mathEditor.insertInto($('#mathEditorContainer #mathEditor')[0]);
    })
  };


  toggleLibrary(){
    this.isShowLibrary = !this.isShowLibrary;
  }

  onToolSelected(event){
    if (event.name == EventService.EventNames.TOOL_SELECTED){
      console.log(event.data);
      const tool = event.data;
      if (tool.id == ToolList.ADD_SECTION.id){
        this.sections.push(new Section(0, "", ""));
        this.sections.forEach((section, i) => {
          section.index = i + 1;
        });
        this.eventService.emitMessageEvent(EventService.EventNames.SECTIONS_CHANGED, this.sections);
        setTimeout(() => {
          // console.log($(`#survey-section-${this.sections.length - 1} .section-buttons button`).
          $(`#survey-section-${this.sections.length - 1} .section-buttons button:first`).click();
        }, 10);
      }
    }
  }

  hideMathEditor(){
    $('#mathEditorContainer').hide();
  }

  applyMathEditor(){
    const latex = this.mathEditor.getMathML();
    this.eventService.emitMessageEvent(EventService.EventNames.MATH_EDITOR_APPLIED, latex);
  }
}
