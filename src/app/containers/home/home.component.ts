import { Component } from '@angular/core';
import { AppBar } from '../../ui';

@Component({
  selector: 'home-container',
  styles: [require('./home.css')],
  template: require('./home.html')
})
export class HomeComponent {}
