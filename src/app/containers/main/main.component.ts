import { Component } from '@angular/core';
import { AppBar } from '../../ui';

@Component({
  selector: 'main-container',
  styles: [ require('./main.css')],
  template: require('./main.html')
})
export class MainComponent {}
