export { MainComponent } from './main/main.component';
export { SurveyComponent } from './survey/survey.component';
export { HomeComponent } from './home/home.component';
export { Notes } from './notes';
export { About } from './about';
export { Auth } from './auth';
    