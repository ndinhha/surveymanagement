export class EventNames{
    public static SECTION_ACTIVE: string = "SECTION_ACTIVE";
    public static TOOL_SELECTED: string = "TOOL_SELECTED";
    public static SECTIONS_CHANGED: string = "SECTIONS_CHANGED";
    public static MATH_EDITOR_APPLIED: string = "MATH_EDITOR_APPLIED";
}