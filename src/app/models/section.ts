import {Question} from './question';

export class Section{
    public questions: Array<Question>;
    constructor(
        public index: number,
        public title: string,
        public description: string
    ){
        this.questions = [];
    }

    public addQuestion(question){
        this.questions.push(question);
        return question;
    }
}