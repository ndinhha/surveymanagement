import {QuestionAnswer} from './question-answer';
import {UUID} from 'angular2-uuid';

export class Question{
    public id: string;
    public type: number;
    public image: string;
    public answers: Array<QuestionAnswer>;
    public content: string;
    public desciption: string;
    public showOptions: any = {
        desciption: false,
        redirect: false,
        shuffle: false
    };

    constructor(id = UUID.UUID()){
        this.id = id;
        this.setType(Question.TYPES.MultipleChoise.value);
        this.answers = [new QuestionAnswer(QuestionAnswer.TYPES.Normal, "Tuỳ chọn 1")];
    }

    public static TYPES = {
        MultipleChoise: {value: 1, name: "Trắc nghiệm"},
        Checkbox: {value: 2, name: "Hộp kiểm"},
        DropdownMenu: {value: 3, name: "Menu thả xuống"}
    }

    public setType(toType){
        this.type = toType;
    }

    public addAnswer(answer){
        const hasOther = this.answers.find(a => a.type == QuestionAnswer.TYPES.Other);
        let other = null;
        if (hasOther){
            other = this.answers.pop();
        }
        if (!hasOther || answer.type != QuestionAnswer.TYPES.Other){
            this.answers.push(answer);
        }
        if (other) this.answers.push(other);
        return answer;
    }
}
