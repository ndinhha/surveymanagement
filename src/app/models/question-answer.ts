import {UUID} from 'angular2-uuid';

export class QuestionAnswer{
    public static TYPES = {
        Normal: 0,
        Other: 1
    };

    public id: string;
    public content: string;
    public image: string;
    public redirect: string;
    public type: number;

    constructor(type: number = QuestionAnswer.TYPES.Normal, content: string = ""){
        this.id = UUID.UUID();
        this.type = type;
        this.content = content;
    }
}