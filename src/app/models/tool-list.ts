export class ToolList{
    public static ADD_QUESTION: any = {id: 'add_question', icon: 'add_circle', title: ''};
    public static ADD_TITLE: any = {id: 'add_title', icon: 'text_fields', title: ''};
    public static ADD_IMAGE: any = {id: 'add_image', icon: 'image', title: ''};
    public static ADD_VIDEO: any = {id: 'add_video', icon: 'video_library', title: ''};
    public static ADD_SECTION: any = {id: 'add_section', icon: 'view_agenda', title: ''};
}