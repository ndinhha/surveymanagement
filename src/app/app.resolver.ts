import * as services from './services';
import { Store } from './store';
import * as uiComponents from './ui';


const mapValuesToArray = (obj) => Object.keys(obj).map(key => obj[key]);

export const APP_RESOLVER = [
  Store,
  ...mapValuesToArray(services)
];

export const APP_UI_COMPONENTS = [
  ...mapValuesToArray(uiComponents)
];
