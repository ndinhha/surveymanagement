import { Component, Input, ElementRef, ViewEncapsulation } from '@angular/core';
import { Question, QuestionAnswer, Section } from '../../models';
import { EventService } from '../../services';

declare var $:any;

@Component({
  selector: 'question-editor',
  styleUrls: ['./question-editor.scss'],
  templateUrl: './question-editor.html',
  encapsulation: ViewEncapsulation.None
})
export class QuestionEditor {
  constructor(
    private eventService: EventService,
    private element: ElementRef
  ){
    this.eventService.subscribe(this.onSectionActive.bind(this));
    setTimeout(()=> {
      this.setActive();
      // $(this.element.nativeElement).find('textarea').focus();
      this.initSortable();
    });
  }

  @Input() section: Section = null;
  @Input() question: Question = new Question();
  @Input() redirectOptions: Array<any> = [];
  @Input() isPreview: Boolean = false;

  isActive: boolean = false;

  questionTypes = [
    Question.TYPES.MultipleChoise,
    Question.TYPES.Checkbox,
    Question.TYPES.DropdownMenu,
  ];

  initSortable(){
    $('.question-answers').sortable({
      axis: 'y',
      items: '.answer:not(.answer-other)',
      handler: '.sortable-button',
      placeholder: 'answer-sortable-placeholder',
      
      start: (e, ui) => {
        $('.answer-sortable-placeholder').css('height', $(ui.item).height());
      },
      update: (e, ui) => {
        // console.log(e, ui);
        // const curAnswerIndex = $(ui.item).data('index');
        // const nextAnswerIndex = $(ui.item).next('.answer') && $(ui.item).next('.answer').data('index') || 0;

        // console.log(this.question.answers);
        // if (curAnswerIndex >= 0){
        //   this.question.answers = [
        //     ...this.question.answers.slice(0, nextAnswerIndex),
        //     this.question.answers[curAnswerIndex],
        //     ...this.question.answers.slice(nextAnswerIndex, curAnswerIndex),
        //     ...this.question.answers.slice(curAnswerIndex + 1)
        //   ];
        // }
        // console.log(this.question.answers);
/**
 * [0,1,2,3,4] 4 -> 0 : 0,0 + 4 + 1,4-1 + 4+1,...
 */
      }
    });
  }

  onSectionActive(event){
    if (event.name == EventService.EventNames.SECTION_ACTIVE){
      this.isActive = false;
    }
  }
  

  setActive(){
    let data = this.element.nativeElement;
    data.question = this.question;
    this.eventService.emitMessageEvent(EventService.EventNames.SECTION_ACTIVE, data)
    this.isActive = true;
    this.isPreview = false;
  }

  removeQuestion(){
    const index = this.section.questions.findIndex(q => q == this.question);
    if (index >= 0){
      this.section.questions.splice(index, 1);
    }
  }

  copyQuestion(){
    this.section.questions.push(Object.assign({}, this.question));
  }

  addAnswer(type){
    const answerType = type == 'other' ? QuestionAnswer.TYPES.Other : QuestionAnswer.TYPES.Normal;
    const answerContent = type == 'other' ? "Khác..." : "Tuỳ chọn " + (this.question.answers.length + 1);
    const answer = this.question.addAnswer(new QuestionAnswer(answerType, answerContent));
    if (answer){
      setTimeout(()=> $(`#answer${answer.id} .answer-content input`).focus());
    }
    setTimeout(()=>this.initSortable(), 50);
  }

  removeAnswer(answer){
    const index = this.question.answers.findIndex(a => a == answer);
    if (index >= 0){
      this.question.answers.splice(index, 1);
    }
  }

  toggleShowOption(option){
    this.question.showOptions[option] = !this.question.showOptions[option];
  }

  hasOther(){
    return this.question.answers.find(a => a.type == QuestionAnswer.TYPES.Other) != null;
  }

  focusAnswer(index){
    return this.question.answers[index] && this.question.answers[index].type == QuestionAnswer.TYPES.Normal
      && index == this.question.answers.length - 1;
  }
}
