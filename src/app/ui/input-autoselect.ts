import {Directive, Input, ElementRef, HostListener} from '@angular/core';

@Directive({
    selector: 'input,textarea'
})
export class InputAutoSelect{
    @HostListener('focus',['$event.target'])
    onFocus(textArea: HTMLTextAreaElement): void {
        setTimeout(()=>this.element.nativeElement.select(), 10);
    }

    constructor(private element: ElementRef){
    }
}