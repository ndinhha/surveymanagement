import { Component, ElementRef, ViewEncapsulation } from '@angular/core';
import { EventService } from '../../services';
import { ToolList } from '../../models';
import $ from 'jquery';

@Component({
  selector: 'editor-toolbar',
  styles: [require('./editor-toolbar.css')],
  template: require('./editor-toolbar.html'),
  encapsulation: ViewEncapsulation.None
})
export class EditorToolbar {

  constructor(
    private eventService: EventService,
    private element: ElementRef
  ){
    this.eventService.subscribe(this.onEventHandler.bind(this));

    // let oldScroll = 0;
    // $(document).on('scroll', (e) =>{
    //   const editorTop = $('.editor').offset().top,
    //     docScrollTop = $(document).scrollTop(),
    //     docHeight = $(document).height(),
    //     activeSection = $('.survey-section.active, .question-editor.active');
    //   const scrollDiff = docScrollTop - oldScroll;
    //   oldScroll = docScrollTop;
    //   let eleTop = ($(this.element.nativeElement).offset().top - editorTop) + scrollDiff;

    //   console.log(editorTop, docScrollTop, scrollDiff, eleTop);

    //   // if (activeSection && (docScrollTop < activeSection.offset().top || )){
    //   //   eleTop = activeSection.offset().top - editorTop;
    //   // }

    //   this.element.nativeElement.style.top = eleTop + 'px';
    // });
    // $(function(){
    // })
  }

  toolList = [
    ToolList.ADD_QUESTION,
    ToolList.ADD_TITLE,
    ToolList.ADD_IMAGE,
    ToolList.ADD_VIDEO,
    ToolList.ADD_SECTION
  ];

  onEventHandler(event){
    if (event.name == EventService.EventNames.SECTION_ACTIVE){
      // console.log($(event.data).offset(), $(event.data));
      // console.log($('.editor').offset());
      setTimeout(()=>{
        this.element.nativeElement.style.top = ($(event.data).offset().top - $('.editor').offset().top) + 'px';
      })
    }
  }

  onSelected(tool){
    this.eventService.emitMessageEvent(EventService.EventNames.TOOL_SELECTED, tool);
  }
}
