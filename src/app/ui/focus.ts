import {Directive, Input, ElementRef, HostListener} from '@angular/core';

@Directive({
    selector: '[focus]'
})
export class Focus{
    @Input() focus: boolean = false;
    constructor(private elementRef: ElementRef){
    }

    protected ngOnChanges(){
        if (this.focus){
            this.elementRef.nativeElement.focus();
        }
    }
}
