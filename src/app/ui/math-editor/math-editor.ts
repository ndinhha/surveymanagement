import {
    Component, Input, ElementRef, HostListener,
    AfterViewInit, OnDestroy
} from '@angular/core';

declare var $:any;
declare var tinymce: any;
declare var MathJax: any;
declare var MediumEditor: any;
declare var MediumButton: any;
declare var typejax: any;

@Component({
    selector: 'math-editor',
    // template: `<div class="math-editor" id="{{elementId}}"></div>`,
    templateUrl: './math-editor.html',
    styles: [`
        .math-editor{
            outline: none;
            min-height: 40px;
            border-bottom: 1px solid #eee;
        }
    `]
})
export class MathEditor implements AfterViewInit, OnDestroy{
    @Input() elementId;
    constructor(private elementRef: ElementRef){
    }

    private editor;
    private editorContainer;
    mathTxt = "";

    private showPreviewMode(){
        this.editorContainer = $(this.elementRef.nativeElement).find('#' + this.elementId);
        this.editorContainer.attr('contenteditable', true);
        this.editorContainer[0].onfocus = this.onFocus.bind(this)
    }


    onFocus(){
        console.log("focus ", Date.now())
        let initEvent = false;
        if (this.editor == null){
            initEvent = true;
            this.editor = new MediumEditor('#' + this.elementId, {
                toolbar: {
                    buttons: ['b', 'u', 'i']
                },
                extensions: {
                    // compact
                    'b':  new MediumButton({
                        label:'<i class="material-icons">format_bold</i>', 
                        start:'\\textbf{', 
                        end:'}',
                        action: (html, mark) => {
                            console.log(html);
                            setTimeout(() => {
                                const latex = this.mathTxt;
                                typejax.updater.init(latex, latex.length, this.editor.elements[0]);
                            });
                            // return '\\textbf{' + html + '}';
                        }
                    }),
                    'u':  new MediumButton({
                        label:'<i class="material-icons">format_underlined</i>', 
                        start:'\\underline{', 
                        end:'}',
                        action: function(html, mark){

                        }
                    }),
                    'i':  new MediumButton({
                        label:'<i class="material-icons">format_italic</i>', 
                        start:'\\textit{', 
                        end:'}',
                        action: function(html, mark){

                        }
                    }),
                    's':  new MediumButton({
                        label:'<i class="material-icons">strikethrough_s</i>', 
                        start:'<b>', 
                        end:'}',
                        action: function(html, mark){

                        }
                    }),

                    // expanded
                    'warning': new MediumButton({
                        label: '<i class="fa fa-exclamation-triangle"></i>',
                        start: '<div class="warning">',
                        end:   '</div>'
                    }),

                    // with JavaScript
                    'pop': new MediumButton({
                        label:'POP',
                        action: function(html, mark, parent){
                            alert('hello :)')
                            return html
                        }
                    })
                }
            });
        }else if (!this.editor.isActive){
            this.editor.setup();
            initEvent = true;
        }

        if (initEvent){
            this.editor.subscribe('focus', editorFocus.bind(this));
            this.editor.subscribe('blur', editorBlur.bind(this));
        }

        function editorFocus(e) {
            console.log(e);
            console.log($(e.target).offset(), $(e.target).width());
            const editorOffset = this.editorContainer.offset(),
                toolbarHeight = 65;

            setTimeout(()=>{
                const editorToolbar = $('.medium-editor-toolbar');
                if (!editorToolbar.is('.medium-editor-toolbar-active')){
                    editorToolbar.css({
                        top: (editorOffset.top - editorToolbar.height() - 5) + 'px',
                        left: editorOffset.left + ((this.editorContainer.width() - editorToolbar.width()) / 2) + 'px'
                    });
                    editorToolbar.addClass('medium-editor-toolbar-active');
                }
            }, 10);
        }

        function editorBlur(e){
            this.editor.destroy();
            this.showPreviewMode();
        }
    }

    ngAfterViewInit() {
        this.showPreviewMode();
    }

    ngOnDestroy() {
        this.editor.destroy();
        this.showPreviewMode();
    }
}
