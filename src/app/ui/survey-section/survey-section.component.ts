import { 
  Component,
  Input,
  Output,
  ElementRef,
  ViewEncapsulation
} from '@angular/core';

declare var $:any;
declare var tinymce: any;
declare var MathJax: any;

import { EventService } from '../../services';
import { Section, ToolList, Question } from '../../models';

@Component({
  selector: 'survey-section',
  styleUrls: ['./survey-section.scss'],
  templateUrl: './survey-section.html',
  encapsulation: ViewEncapsulation.None
})
export class SurveySection {
  @Input() section: Section = new Section(0, "", "");
  @Input() sections: Array<Section> = [];
  @Input() isCommonSection: boolean = false;
  
  isPreview: boolean = true;
  isActive: boolean = false;
  isLastSection: boolean = false;
  redirectOptions: Array<any> = [
    {name: "Tiếp tục tới phần tiếp theo", value: "next_section"}
  ];
  selectedRedirect: string = this.redirectOptions[0].value;
  currentQuestion: Question;

  redirectConditions = [
    {name: "Mọi trường hợp", value: "1"},
    {name: "Đúng 1 câu", value: "2"},
    {name: "Đúng 2 câu", value: "3"},
    {name: "Đúng 3 câu", value: "4"}
  ];
  selectedRedirectCondition = this.redirectConditions[0].value;

  constructor(
    private eventService: EventService,
    private element: ElementRef
  ){
    this.eventService.subscribe(this.onEventHandler.bind(this));
    this.isLastSection = this.sections.findIndex(s => s == this.section) == this.sections.length - 1;

    setTimeout(()=>{
      this.redirectOptions = [
        {name: "Tiếp tục tới phần tiếp theo", value: "next_section"}
      ].concat(this.sections.map(section => {
        return {name: `Chuyển đến bài ${section.index} (${section.title || 'Không tiêu đề'})`, value: "goto_section_" + section.index};
      }));
      this.initSortable();
    })
  }

  // editor;

  // ngAfterViewInit() {
  //   tinymce.init({
  //     selector: '#surveyTitle',
  //     inline: true,
  //     toolbar: 'undo redo',
  //     menubar: false,
  //     skin_url: 'assets/skins/lightgray',
  //     setup: editor => {
  //       this.editor = editor;
  //       editor.on('keyup', () => {
  //         const content = editor.getContent();
  //         console.log(content);
  //         MathJax.Hub.Queue(["Typeset", MathJax.Hub, $('#surveyTitle')[0]]);
  //         // this.onEditorKeyup.emit(content);
  //       });
  //     },
  //   });
  // }

  onEventHandler(event){
    
    if (event.name == EventService.EventNames.SECTION_ACTIVE){
      this.isActive = false;
      this.currentQuestion = event.data.question;
    }else if (event.name == EventService.EventNames.TOOL_SELECTED){
      const tool = event.data;
      const isActiveSection = this.isActive || this.currentQuestion && this.section.questions.find(q => q == this.currentQuestion);
      if (!this.isCommonSection && isActiveSection && tool.id == ToolList.ADD_QUESTION.id){
        const question = this.section.addQuestion(new Question());
        setTimeout(()=>{
          this.initSortable();
          $(`#question${question.id} .question-header textarea`).focus();
        }, 10);
      }
    }else if (event.name == EventService.EventNames.SECTIONS_CHANGED){
      this.redirectOptions = [
        {name: "Tiếp tục tới phần tiếp theo", value: "next_section"}
      ].concat(event.data.map(section => {
        return {name: `Chuyển đến bài ${section.index} (${section.title || 'Không tiêu đề'})`, value: "goto_section_" + section.index};
      }));

      this.isLastSection = this.sections.findIndex(s => s == this.section) == this.sections.length - 1;
    }
  }

  initSortable(){
    $('.survey-container').sortable({
      axis: 'y',
      items: 'question-editor',
      handler: 'question-editor >.sortable-button',
      placeholder: 'question-sortable-placeholder',
      connectWith: '.survey-section:not(.common-section) .survey-container',

      start: (e, ui) => {
        $('.question-sortable-placeholder').css('height', $(ui.item).height());
      },
      stop: (e, ui) => {
        console.log(e, ui);
      }
    });
  }

  onChangeTitle(){
    this.eventService.emitMessageEvent(EventService.EventNames.SECTIONS_CHANGED, this.sections);    
  }

  togglePreview(){
    this.eventService.emitMessageEvent(EventService.EventNames.SECTION_ACTIVE, this.element.nativeElement);
    this.isPreview = !this.isPreview;
    this.isActive = true;
  }

  focusSection(){
    this.eventService.emitMessageEvent(EventService.EventNames.SECTION_ACTIVE, this.element.nativeElement);
    this.isPreview = false;
    this.isActive = true;
  }

  removeSection(){
    const index = this.sections.findIndex(s => s == this.section);
    if (index >= 0){
      this.sections.splice(index, 1);
      this.reOrderSection();
      this.eventService.emitMessageEvent(EventService.EventNames.SECTIONS_CHANGED, this.sections);
    }
  }

  copySection(){
    this.sections.push(Object.assign({}, this.section));
    this.reOrderSection();
  }

  mergeSection(){
    const index = this.sections.findIndex(s => s == this.section);
    if (index > 0){
      this.sections[index - 1].questions = this.sections[index - 1].questions.concat(this.section.questions);
      this.sections.splice(index, 1);
      this.reOrderSection();
    }
  }

  // Private method
  private reOrderSection(){
    this.sections.forEach((section, i) => {
      section.index = i + 1;
    });
  }
}
