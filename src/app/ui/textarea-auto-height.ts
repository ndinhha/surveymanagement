import {Directive, Input, ElementRef, HostListener} from '@angular/core';

@Directive({
    selector: 'textarea[autoheight]'
})
export class TextareaAutoHeight{
    @HostListener('input',['$event.target'])
    onInput(textArea: HTMLTextAreaElement): void {
        this.adjust();
    }
    
    constructor(public element: ElementRef){
        const rows = this.element.nativeElement.attributes['rows'] || 1;
        this.element.nativeElement.setAttribute('rows', rows);
        this.element.nativeElement.style['resize'] = 'none';
        this.element.nativeElement.style['overflow'] = 'hidden';
        this.element.nativeElement.style['height'] = 'auto';
    }

    ngAfterContentChecked(): void{
        this.adjust();
    }

    adjust(){
        this.element.nativeElement.style.overflow = 'hidden';
        this.element.nativeElement.style.height = 'auto';
        this.element.nativeElement.style.height = this.element.nativeElement.scrollHeight + "px";
    }
}