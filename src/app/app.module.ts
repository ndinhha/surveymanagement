import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule }   from '@angular/forms';
import { HttpModule }    from '@angular/http';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {
  MaterialModule,
  MdButtonModule, 
  MdCheckboxModule,
  MdInputModule,
  MdRadioModule,
  MdSelectModule,
  MdIconModule,
  MdMenuModule
} from '@angular/material';

import { AppRoutingModule } from './routes';
import { AppComponent } from './app.component';
import {
    MainComponent,
    SurveyComponent,
    HomeComponent,
    About,
    Auth,
    Notes
} from './containers';
// import {
//   AppBar,
//   EditorToolbar,
//   SurveySection,
//   Focus,
//   QuestionEditor,
//   ColorPicker,
//   NoteCard,
//   NoteCreator
// } from './ui';
// import * as uiComponents from './ui';

import { APP_RESOLVER, APP_UI_COMPONENTS } from './app.resolver';
import 'hammerjs'
import 'tinymce/tinymce';
// import 'tinymce/themes/modern/theme.js';
// import 'tinymce/plugins/link/plugin.js';
// import 'tinymce/plugins/paste/plugin.js';
// import 'tinymce/plugins/table/plugin.js';
import 'medium-editor/dist/js/medium-editor';
// import 'medium-editor/dist/css/medium-editor.min.css';
// import 'medium-editor/dist/css/themes/flat.min.css';




@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    AppRoutingModule,
    MaterialModule
  ],
  declarations: [
    AppComponent,
    MainComponent,
    HomeComponent,
    SurveyComponent,
    About,
    Auth,
    Notes,
    
    ...APP_UI_COMPONENTS
  ],
  providers: [ APP_RESOLVER ],
  bootstrap: [ AppComponent ]
})
export class AppModule { }
