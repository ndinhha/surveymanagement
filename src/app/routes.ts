import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainComponent,
  HomeComponent,
  SurveyComponent,
  Notes,
  About,
  Auth 
} from './containers';
import { AuthService } from './services';

const routes: Routes = [
  {
    path: '',
    component: MainComponent,
    children: [
      { path: '', component: HomeComponent },
      { path: 'survey', component: SurveyComponent },
      { path: 'about', component: SurveyComponent },

    ]
  },
  { path: 'auth', component: Auth },
  { path: '**', redirectTo: 'index' }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
