import {EventEmitter, Injectable} from '@angular/core';
import {EventNames, Message} from '../models';

@Injectable()
export class EventService{
    public static EventNames = EventNames;
    private dispatcher: EventEmitter<any> = new EventEmitter();
    private subscriptions: Array<any> = [];

    constructor(){}

    emitMessageEvent(name:string, data:any){
        let message = new Message(name, data);
        this.dispatcher.emit(message);
    }
  
    subscribe(handler: any){
        const subscription = this.dispatcher.subscribe(handler);
        this.subscriptions.push([subscription, handler]);
    }

    unsubscribe(handler: any){
        this.subscriptions.forEach(({subscription, h}) => {
            if (h === handler){
                subscription.unsubscribe();
            }
        });
    }
}